#! /usr/bin/python3

import socket
import logging
import sys
import os
import traceback
import select
from sys import stdin

PORT = 1883
TYPE_PUBLISH = 0x30
TYPE_CONNECT = 0x10 #Notice the connection (0x10)
TYPE_CONNACK = 0x20 #connection acknowledge (0x20) control codes.
TYPE_SUBSCRIBE = 0x80
TYPE_SUBACK = 0x90
TYPE_DISCONNECT = 0xe0 

def create_mqtt_publish_msg(topic, value, retain=False):
    retain_code = 0
    if retain:
        retain_code = 1
    # 0011 0000 : Message Type = Publish ; Dup Flag = 0 ; QoS = 0
    msg_mqtt_flags = (TYPE_PUBLISH + retain_code).to_bytes(1, byteorder='big')
    msg_topic = topic.encode("ascii")
    msg_value = bytes(value, "ascii")
    msg_topic_length = len(msg_topic).to_bytes(2, byteorder='big')
    msg = msg_topic_length + msg_topic + msg_value
    msg_length = len(msg).to_bytes(1, byteorder='big')
    return msg_mqtt_flags + msg_length + msg

def msg_connect(pub_id):
    connect_type = (TYPE_CONNECT).to_bytes(1, byteorder='big')
    protocol_name= b'\x04MQTT'
    protocol_level=b'\x04'
    connect_flags= b'\x02'
    keep_alive=b'\x00'
    vheaderp= protocol_name+protocol_level+connect_flags+keep_alive
    payload= pub_id.encode("UTF-8")
    lenght=(len(vheaderp)+len(payload)).to_bytes(1, byteorder='big')
    msg =connect_type + lenght +vheaderp + payload
    return msg  

def msg_disconnect():
    connect_type = (TYPE_DISCONNECT).to_bytes(1, byteorder='big')  
    remaining_length=b'\x00'
    msg= connect_type + remaining_length
    return msg

msg_disconnect()

def msg_connack():
    connect_type = (TYPE_CONNACK).to_bytes(1, byteorder='big')
    remaining_lenght=b'\x02'
    msg= connect_type + remaining_lenght
    return(msg)

msg_connack()

def msg_subscribe(topic,packet_id):
   connect_type = (TYPE_SUBSCRIBE).to_bytes(1, byteorder='big')
   packet_identifier=b'\x00\x0a'
   payloadml=b'\x00\03'
   payload_topic=topic.encode("UTF-8")
   payload=payloadml+payload_topic
   length=(len(packet_identifier)+len(payload)).to_bytes(1, byteorder='big')
   msg= connect_type + length + packet_identifier + payload 
   return msg


def msg_suback():
    connect_type = (TYPE_SUBACK).to_bytes(1, byteorder='big')
    msb=b'\x00'
    lsb=b'\x01'
    identifier=msb+lsb
    payload=b'\x00'
    msg_lenght=(len(identifier)+len(payload)).to_bytes(1, byteorder='big')
    msg=connect_type + msg_lenght + identifier +payload 
    return msg 

def fonction_decode(msg):
    type= msg[0:1]
    taile=msg[1:3]
    topicl=msg[3:4]
    dtopicl=int.from_bytes(topicl,"big")
    topic=msg[4:4+dtopicl].decode("utf-8")
    valeur=msg[4+dtopicl:].decode("utf-8")
    return topic,valeur






from sys import stdin

def run_publisher(addr, topic, pub_id, retain=False):
    s=socket.socket(socket.AF_INET6 , socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        s.connect(addr)
        connect=msg_connect(pub_id)
        print("connect sent",connect)
        s.sendto(connect,addr)
        while True:
            msgConack = s.recv(1024)
            if msgConack [0:1]== b'\x20':
                print("success")
                for line in stdin:
                    msg = line[:-1]       # on supprime le retour à la ligne ('\n')
                    pkt = create_mqtt_publish_msg(topic, msg, retain)
                    s.sendto(pkt,addr)
                    print("send publish")
            s.sendto(msg_disconnect(),addr)
            print("disconnect")
            s.close()

    except:
        s.sendto(msg_disconnect(),addr)
        print("disconnect")
        s.close()

        
    
def run_subscriber(addr, topic, sub_id): 
    s=socket.socket(socket.AF_INET , socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        s.connect(addr)


        connect=msg_connect(sub_id)
        s.sendall(connect)
        print("send connect")
        msgConack = s.recv(1024)
        if msgConack[0:1]== b'\x20':
            s.sendall(msg_subscribe(topic,sub_id))
            print("envoie sub ")
            msgSuback = s.recv(1024)
            if  msgSuback[0:1]== b'\x90':
                print("suback recue")
                while True:
                    data=s.recv(1024)
                    if data[0:1]==b'\x30':
                        print("msg recue")
                        topic2,valeur=fonction_decode(data)
                        if topic2==topic:
                            print(valeur)
                    elif data==b"" or data==b"\n": break
        s.sendall(msg_disconnect())
        s.close
    except :
        s.sendto(msg_disconnect(),addr)
        print("disconnect")
        s.close()



def run_server(addr):
    s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)
    s.listen(1)
    l = []
    abbonement=[]
    while True:
        r, _, _ = select.select(l + [s], [], [])
        for s2 in r:
            if s2 == s:
                s3, a = s.accept()
                print("new client:", a)
                l = l + [s3]
            else:
                msg = s2.recv(1500)
                if len(msg) == 0:
                    print("client disconnected")
                    s2.close()
                    l.remove(s2)
                elif msg[0:1]==b'\x10':
                    msgConack=msg_connack()
                    print("connect recue")
                    s2.sendall(msgConack)
                elif msg[0:1]==b'\x80':
                    msgSuback=msg_suback()
                    print("subscribe recue")
                    s2.sendall(msgSuback)
                    abbonement=abbonement+[s2]
                elif msg[0:1]==b'\x30':
                    for s3 in abbonement:
                        s3.sendall(msg)
                        print(msg)
                    print("publish recue")
                elif msg[0:1]== b'\xe0':
                    print("disconnected")
                    l.remove(s2)
                else:
                    print("mauvais msg")

      



        
